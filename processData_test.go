package main

import (
	"reflect"
	"testing"
)

func TestValidateData(t *testing.T) {
	tests := []struct {
		name     string
		request  MyRequest
		expected bool
	}{
		{
			name: "ValidData",
			request: MyRequest{
				N: 2,
				M: 2,
				Digits: []Node{
					{I: 0, J: 0, Value: 1},
					{I: 1, J: 1, Value: 2},
				},
			},
			expected: true,
		},
		{
			name: "NegativeValue",
			request: MyRequest{
				N: 2,
				M: 2,
				Digits: []Node{
					{I: 0, J: 0, Value: -1},
				},
			},
			expected: false,
		},
		{
			name: "IndexOutOfBounds",
			request: MyRequest{
				N: 2,
				M: 2,
				Digits: []Node{
					{I: 2, J: 0, Value: 1},
				},
			},
			expected: false,
		},
		{
			name: "ZeroDimensions",
			request: MyRequest{
				N: 0,
				M: 0,
				Digits: []Node{
					{I: 0, J: 0, Value: 1},
				},
			},
			expected: false,
		},
		{
			name: "EmptyDigits",
			request: MyRequest{
				N:      2,
				M:      2,
				Digits: []Node{},
			},
			expected: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result := ValidateData(tt.request)
			if result != tt.expected {
				t.Errorf("ValidateData(%v) = %v, expected %v", tt.request, result, tt.expected)
			}
		})
	}
}

func TestCreateMatrix(t *testing.T) {
	tests := []struct {
		name     string
		matrix   Matrix
		expected [][]int
	}{
		{
			name: "BasicMatrix",
			matrix: Matrix{
				MatrixLength: 2,
				MatrixHeight: 2,
				MatrixNodes: []Node{
					{I: 0, J: 0, Value: 1},
					{I: 1, J: 1, Value: 2},
				},
			},
			expected: [][]int{
				{1, 0},
				{0, 2},
			},
		},
		{
			name: "LargerMatrix",
			matrix: Matrix{
				MatrixLength: 3,
				MatrixHeight: 3,
				MatrixNodes: []Node{
					{I: 0, J: 0, Value: 1},
					{I: 1, J: 1, Value: 2},
					{I: 2, J: 2, Value: 3},
				},
			},
			expected: [][]int{
				{1, 0, 0},
				{0, 2, 0},
				{0, 0, 3},
			},
		},
		{
			name: "EmptyMatrix",
			matrix: Matrix{
				MatrixLength: 2,
				MatrixHeight: 2,
				MatrixNodes:  []Node{},
			},
			expected: [][]int{
				{0, 0},
				{0, 0},
			},
		},
		{
			name: "SingleElementMatrix",
			matrix: Matrix{
				MatrixLength: 1,
				MatrixHeight: 1,
				MatrixNodes: []Node{
					{I: 0, J: 0, Value: 42},
				},
			},
			expected: [][]int{
				{42},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.matrix.CreateMatrix()
			if !reflect.DeepEqual(tt.matrix.Matrix, tt.expected) {
				t.Errorf("CreateMatrix() = %v, expected %v", tt.matrix.Matrix, tt.expected)
			}
		})
	}
}

func TestIsValidMove(t *testing.T) {
	tests := []struct {
		name     string
		matrix   Matrix
		row, col int
		number   int
		visited  [][]bool
		expected bool
	}{
		{
			name: "ValidMove",
			matrix: Matrix{
				MatrixLength: 3,
				MatrixHeight: 3,
				Matrix: [][]int{
					{0, 3, 0},
					{0, 0, 5},
					{0, 0, 1},
				},
			},
			row:    1,
			col:    1,
			number: 5,
			visited: [][]bool{
				{false, false, false},
				{false, false, false},
				{false, false, false},
			},
			expected: true,
		},
		{
			name: "InvalidMoveDueToSameNumberAbove",
			matrix: Matrix{
				MatrixLength: 3,
				MatrixHeight: 3,
				Matrix: [][]int{
					{0, 3, 0},
					{0, 0, 3},
					{2, 0, 1},
				},
			},
			row:    1,
			col:    1,
			number: 3,
			visited: [][]bool{
				{false, false, false},
				{false, false, false},
				{false, false, false},
			},
			expected: false,
		},
		{
			name: "InvalidMoveDueToSameNumberRight",
			matrix: Matrix{
				MatrixLength: 3,
				MatrixHeight: 3,
				Matrix: [][]int{
					{0, 3, 0},
					{0, 4, 0},
					{0, 0, 2},
				},
			},
			row:    1,
			col:    0,
			number: 4,
			visited: [][]bool{
				{false, false, false},
				{false, false, false},
				{false, false, false},
			},
			expected: false,
		},
		{
			name: "InvalidMoveDueToSameNumberBelow",
			matrix: Matrix{
				MatrixLength: 3,
				MatrixHeight: 3,
				Matrix: [][]int{
					{0, 3, 0},
					{0, 0, 5},
					{0, 5, 1},
				},
			},
			row:    0,
			col:    2,
			number: 5,
			visited: [][]bool{
				{false, false, false},
				{false, false, false},
				{false, false, false},
			},
			expected: false,
		},
		{
			name: "InvalidMoveDueToSameNumberLeft",
			matrix: Matrix{
				MatrixLength: 3,
				MatrixHeight: 3,
				Matrix: [][]int{
					{0, 3, 0},
					{0, 0, 5},
					{0, 0, 1},
				},
			},
			row:    0,
			col:    2,
			number: 3,
			visited: [][]bool{
				{false, false, false},
				{false, false, false},
				{false, false, false},
			},
			expected: false,
		},
		{
			name: "InvalidMoveDueToVisitedCell",
			matrix: Matrix{
				MatrixLength: 3,
				MatrixHeight: 3,
				Matrix: [][]int{
					{0, 3, 0},
					{0, 0, 5},
					{0, 0, 1},
				},
			},
			row:    1,
			col:    1,
			number: 5,
			visited: [][]bool{
				{false, false, false},
				{false, true, false},
				{false, false, false},
			},
			expected: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result := tt.matrix.IsValidMove(tt.row, tt.col, tt.number, tt.visited)
			if result != tt.expected {
				t.Errorf("IsValidMove() = %v, expected %v", result, tt.expected)
			}
		})
	}
}

func TestCheckConditions(t *testing.T) {
	tests := []struct {
		name     string
		matrix   Matrix
		expected bool
	}{
		{
			name: "ConditionMet",
			matrix: Matrix{
				MatrixLength: 3,
				MatrixHeight: 3,
				Matrix: [][]int{
					{1, 2, 3},
					{4, 5, 6},
					{7, 8, 9},
				},
				Conditions: []Condition{
					{X1: 0, Y1: 0, X2: 1, Y2: 1, Sum: 12}, // 1+2+4+5=12
				},
			},
			expected: true,
		},
		{
			name: "ConditionNotMet",
			matrix: Matrix{
				MatrixLength: 3,
				MatrixHeight: 3,
				Matrix: [][]int{
					{1, 2, 3},
					{4, 5, 6},
					{7, 8, 9},
				},
				Conditions: []Condition{
					{X1: 0, Y1: 0, X2: 1, Y2: 1, Sum: 10}, // 1+2+4+5 != 10
				},
			},
			expected: false,
		},
		{
			name: "MultipleConditionsOneMet",
			matrix: Matrix{
				MatrixLength: 3,
				MatrixHeight: 3,
				Matrix: [][]int{
					{1, 2, 3},
					{4, 5, 6},
					{7, 8, 9},
				},
				Conditions: []Condition{
					{X1: 0, Y1: 0, X2: 0, Y2: 0, Sum: 1},  // 1 == 1
					{X1: 1, Y1: 1, X2: 2, Y2: 2, Sum: 30}, // 5+6+8+9 != 30
				},
			},
			expected: true,
		},
		{
			name: "EmptyConditions",
			matrix: Matrix{
				MatrixLength: 3,
				MatrixHeight: 3,
				Matrix: [][]int{
					{1, 2, 3},
					{4, 5, 6},
					{7, 8, 9},
				},
				Conditions: []Condition{},
			},
			expected: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result := tt.matrix.CheckConditions()
			if result != tt.expected {
				t.Errorf("CheckConditions() = %v, expected %v", result, tt.expected)
			}
		})
	}
}

func TestRender(t *testing.T) {
	tests := []struct {
		name       string
		matrix     Matrix
		nodeNumber int
		expected   bool
	}{
		{
			name: "SingleNodeRender",
			matrix: Matrix{
				MatrixLength: 3,
				MatrixHeight: 3,
				MatrixNodes: []Node{
					{I: 0, J: 0, Value: 1},
				},
			},
			nodeNumber: 0,
			expected:   true,
		},
		{
			name: "MultipleNodesRender",
			matrix: Matrix{
				MatrixLength: 3,
				MatrixHeight: 3,
				MatrixNodes: []Node{
					{I: 0, J: 0, Value: 1},
					{I: 1, J: 1, Value: 2},
				},
			},
			nodeNumber: 1,
			expected:   false,
		},
		{
			name: "InvalidNodeRender",
			matrix: Matrix{
				MatrixLength: 3,
				MatrixHeight: 3,
				MatrixNodes: []Node{
					{I: 0, J: 0, Value: 1},
				},
			},
			nodeNumber: 1,
			expected:   false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.matrix.CreateMatrix()
			result := tt.matrix.Render(tt.nodeNumber)
			if result != tt.expected {
				t.Errorf("Render() = %v, expected %v", result, tt.expected)
			}
		})
	}
}

func TestFillMatrix(t *testing.T) {
	tests := []struct {
		name       string
		matrix     Matrix
		i, j       int
		nodeNumber int
		counter    int
		expected   bool
	}{
		{
			name: "SimpleCase",
			matrix: Matrix{
				MatrixLength: 3,
				MatrixHeight: 3,
				MatrixNodes: []Node{
					{I: 0, J: 0, Value: 1},
				},
			},
			i:          0,
			j:          0,
			nodeNumber: 0,
			counter:    1,
			expected:   true,
		},
		{
			name: "ComplexCase",
			matrix: Matrix{
				MatrixLength: 3,
				MatrixHeight: 3,
				MatrixNodes: []Node{
					{I: 0, J: 0, Value: 1},
					{I: 1, J: 1, Value: 2},
				},
			},
			i:          0,
			j:          0,
			nodeNumber: 1,
			counter:    2,
			expected:   false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.matrix.CreateMatrix()
			visited := make([][]bool, tt.matrix.MatrixLength)
			for i := range visited {
				visited[i] = make([]bool, tt.matrix.MatrixHeight)
			}
			result := tt.matrix.FillMatrix(tt.i, tt.j, tt.nodeNumber, tt.counter, visited)
			if result != tt.expected {
				t.Errorf("FillMatrix() = %v, expected %v", result, tt.expected)
			}
		})
	}
}
