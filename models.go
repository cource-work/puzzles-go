package main

type Node struct {
	I     int `json:"i"`
	J     int `json:"j"`
	Value int `json:"value"`
}

type Condition struct {
	X1  int `json:"x1"`
	Y1  int `json:"y1"`
	X2  int `json:"x2"`
	Y2  int `json:"y2"`
	Sum int `json:"sum"`
}

type MyRequest struct {
	N          int         `json:"n"`
	M          int         `json:"m"`
	Digits     []Node      `json:"digits"`
	Conditions []Condition `json:"conditions"`
}

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
}
