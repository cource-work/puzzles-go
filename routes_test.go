package main

import (
	"bytes"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRoutes(t *testing.T) {
	// Create a new Gin router instance
	r := gin.Default()

	// Define routes
	r.POST("/", createUser)
	r.POST("/login", login)

	authenticated := r.Group("/")
	authenticated.Use(authenticateUser())
	{
		//authenticated.GET("/get-result/:id_item", getDataAndResultFromDatabase)
		//authenticated.GET("/generate", getGeneratedData)
		//authenticated.POST("/process_data", processData)
	}

	initDb()
	// Create a test HTTP server
	ts := httptest.NewServer(r)
	defer ts.Close()

	// Test createUser endpoint
	createUserURL := ts.URL + "/"
	createUserReq, err := http.NewRequest("POST", createUserURL, nil)
	if err != nil {
		t.Fatal(err)
	}
	createUserResp, err := http.DefaultClient.Do(createUserReq)
	if err != nil {
		t.Fatal(err)
	}
	if createUserResp.StatusCode != http.StatusBadRequest {
		t.Errorf("expected status %d, got %d", http.StatusBadRequest, createUserResp.StatusCode)
	}
	createUserData := map[string]string{
		"username": "test_user",
		"password": "test_password",
	}
	createUserDataJSON, _ := json.Marshal(createUserData)
	createUserReq, err = http.NewRequest("POST", createUserURL, bytes.NewBuffer(createUserDataJSON))
	if err != nil {
		t.Fatal(err)
	}
	createUserReq.Header.Set("Content-Type", "application/json")
	createUserResp, err = http.DefaultClient.Do(createUserReq)
	if err != nil {
		t.Fatal(err)
	}
	if createUserResp.StatusCode != http.StatusCreated {
		t.Errorf("expected status %d, got %d", http.StatusCreated, createUserResp.StatusCode)
	}

	// Test login endpoint
	loginURL := ts.URL + "/login"
	loginReq, err := http.NewRequest("POST", loginURL, nil)
	if err != nil {
		t.Fatal(err)
	}
	loginResp, err := http.DefaultClient.Do(loginReq)
	if err != nil {
		t.Fatal(err)
	}
	if loginResp.StatusCode != http.StatusBadRequest {
		t.Errorf("expected status %d, got %d", http.StatusBadRequest, loginResp.StatusCode)
	}
	loginData := []byte(`{"username": "test_user", "password": "test_password"}`)
	req, err := http.NewRequest("POST", "/login", bytes.NewBuffer(loginData))
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()

	// Create a mock Gin context
	c, _ := gin.CreateTestContext(w)
	c.Request = req

	// Call the handler function
	login(c)

	if w.Code != http.StatusOK {
		t.Errorf("expected status %d, got %d", http.StatusOK, loginResp.StatusCode)
	}

	//getResultURL := ts.URL + "/get-result/123"
	//getResultReq, err := http.NewRequest("GET", getResultURL, nil)
	//if err != nil {
	//	t.Fatal(err)
	//}
	//getResultReq.Header.Set("Authorization", "Bearer mock-token")
	//getResultResp, err := http.DefaultClient.Do(getResultReq)
	//if err != nil {
	//	t.Fatal(err)
	//}
	//if getResultResp.StatusCode != http.StatusOK {
	//	t.Errorf("expected status %d, got %d", http.StatusOK, getResultResp.StatusCode)
	//}
	//
	//// Example: Test processData
	//processDataURL := ts.URL + "/process_data"
	//processDataBody := []byte(`{"data": "example"}`)
	//processDataReq, err := http.NewRequest("POST", processDataURL, bytes.NewBuffer(processDataBody))
	//if err != nil {
	//	t.Fatal(err)
	//}
	//processDataReq.Header.Set("Content-Type", "application/json")
	//processDataResp, err := http.DefaultClient.Do(processDataReq)
	//if err != nil {
	//	t.Fatal(err)
	//}
	//if processDataResp.StatusCode != http.StatusOK {
	//	t.Errorf("expected status %d, got %d", http.StatusOK, processDataResp.StatusCode)
	//}

}
