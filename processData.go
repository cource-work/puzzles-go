package main

func ValidateData(request MyRequest) bool {
	for _, value := range request.Digits {
		if value.I > request.N-1 || value.J > request.M-1 || value.Value < 0 {
			return false
		}
	}
	return request.N > 0 && request.M > 0 && len(request.Digits) > 0
}

type Matrix struct {
	MatrixLength int         `json:"n"`
	MatrixHeight int         `json:"m"`
	MatrixNodes  []Node      `json:"digits"`
	Conditions   []Condition `json:"conditions"`
	Matrix       [][]int     `json:"solving"`
	Reason       string      `json:"reason"`
}

func (m *Matrix) InitializeVariables(request MyRequest) bool {
	if ValidateData(request) {
		m.MatrixNodes = request.Digits
		m.MatrixLength = request.N
		m.MatrixHeight = request.M
		m.Conditions = request.Conditions
		return true
	}
	m.Reason = "Data is invalid."
	return false
}

func (m *Matrix) ProcessData() ([][]int, string) {
	m.CreateMatrix()

	startMatrix := make([][]int, len(m.Matrix))
	for i := range m.Matrix {
		startMatrix[i] = make([]int, len(m.Matrix[i]))
		copy(startMatrix[i], m.Matrix[i])
	}

	if m.Render(0) {
		return startMatrix, ""
	} else {
		m.Reason = "The arrangement of numbers or conditions for sum does not allow solving the problem"
		return startMatrix, m.Reason
	}
}

func (m *Matrix) CreateMatrix() {
	m.Matrix = make([][]int, m.MatrixLength)
	for i := range m.Matrix {
		m.Matrix[i] = make([]int, m.MatrixHeight)
	}
	for _, node := range m.MatrixNodes {
		m.Matrix[node.I][node.J] = node.Value
	}
}

func (m *Matrix) Render(nodeNumber int) bool {
	if nodeNumber > len(m.MatrixNodes)-1 {
		return false
	}
	curNode := m.MatrixNodes[nodeNumber]
	visited := make([][]bool, m.MatrixLength, m.MatrixHeight)
	for i := range visited {
		visited[i] = make([]bool, m.MatrixHeight)
	}
	visited[curNode.I][curNode.J] = true

	if !m.FillMatrix(curNode.I, curNode.J, nodeNumber, curNode.Value, visited) {
		return false
	}
	if nodeNumber == len(m.MatrixNodes)-1 {
		return true
	}
	return false
}

func (m *Matrix) FillMatrix(i, j, nodeNumber, counter int, visited [][]bool) bool {
	value := m.MatrixNodes[nodeNumber].Value
	if counter > 1 {
		if j-1 >= 0 && m.Matrix[i][j-1] == 0 && m.IsValidMove(i, j-1, value, visited) {
			m.Matrix[i][j-1] = value
			counter--
			visited[i][j-1] = true
			if !m.FillMatrix(i, j-1, nodeNumber, counter, visited) {
				m.Matrix[i][j-1] = 0
				counter++
				visited[i][j-1] = false
			}
		}
		if i-1 >= 0 && m.Matrix[i-1][j] == 0 && m.IsValidMove(i-1, j, value, visited) {
			m.Matrix[i-1][j] = value
			counter--
			visited[i-1][j] = true
			if !m.FillMatrix(i-1, j, nodeNumber, counter, visited) {
				m.Matrix[i-1][j] = 0
				counter++
				visited[i-1][j] = false
			}
		}
		if j+1 < m.MatrixLength-1 && m.Matrix[i][j+1] == 0 && m.IsValidMove(i, j+1, value, visited) {
			m.Matrix[i][j+1] = value
			counter--
			visited[i][j+1] = true
			if !m.FillMatrix(i, j+1, nodeNumber, counter, visited) {
				m.Matrix[i][j+1] = 0
				counter++
				visited[i][j+1] = false
			}
		}
		if i+1 < m.MatrixHeight-1 && m.Matrix[i+1][j] == 0 && m.IsValidMove(i+1, j, value, visited) {
			m.Matrix[i+1][j] = value
			counter--
			visited[i+1][j] = true
			if !m.FillMatrix(i+1, j, nodeNumber, counter, visited) {
				m.Matrix[i+1][j] = 0
				counter++
				visited[i+1][j] = false
			}
		}
		return false
	}
	if nodeNumber == len(m.MatrixNodes)-1 {
		return true
	}
	if counter <= 1 {
		if m.CheckConditions() {
			m.Render(nodeNumber + 1)
		} else {
			return false
		}
	}
	return false
}

func (m *Matrix) IsValidMove(row, col, number int, visited [][]bool) bool {
	if row-1 >= 0 && m.Matrix[row-1][col] == number && !visited[row-1][col] {
		return false
	}
	if col+1 < m.MatrixLength-1 && m.Matrix[row][col+1] == number && !visited[row][col+1] {
		return false
	}
	if row+1 < m.MatrixHeight-1 && m.Matrix[row+1][col] == number && !visited[row+1][col] {
		return false
	}
	if col-1 >= 0 && m.Matrix[row][col-1] == number && !visited[row][col-1] {
		return false
	}
	return true
}

func (m *Matrix) CheckConditions() bool {
	for _, condition := range m.Conditions {
		curSum := 0
		for i := condition.Y1; i <= condition.Y2; i++ {
			for j := condition.X1; j <= condition.X2; j++ {
				curSum += m.Matrix[i][j]
			}
		}
		if curSum == condition.Sum {
			return true
		}
	}
	return false
}
