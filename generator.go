package main

import (
	"math/rand"
	"time"
)

func generateData(maxID int) map[string]interface{} {
	rand.Seed(time.Now().UnixNano())
	n := rand.Intn(9) + 2
	m := rand.Intn(9) + 2
	sumOfValues := n * m
	var digits []Node
	matrix := make([][]int, n)
	for i := range matrix {
		matrix[i] = make([]int, m)
	}

	for sumOfValues > 0 {
		i := rand.Intn(n)
		j := rand.Intn(m)
		value := rand.Intn(sumOfValues/2+1) + 1
		if matrix[i][j] == 0 {
			validValue := true
			if i > 0 && matrix[i-1][j] == value {
				validValue = false
			}
			if i < n-1 && matrix[i+1][j] == value {
				validValue = false
			}
			if j > 0 && matrix[i][j-1] == value {
				validValue = false
			}
			if j < m-1 && matrix[i][j+1] == value {
				validValue = false
			}
			if validValue {
				digits = append(digits, Node{I: i, J: j, Value: value})
				matrix[i][j] = value
				sumOfValues -= value
			}
		}
	}

	mat := Matrix{}
	request := MyRequest{
		N:          n,
		M:          m,
		Digits:     digits,
		Conditions: []Condition{},
	}
	var result interface{}
	if mat.InitializeVariables(request) {
		_, result = mat.ProcessData()
	}

	return map[string]interface{}{
		"n":          n,
		"m":          m,
		"digits":     digits,
		"conditions": []Condition{},
		"id_item":    maxID + 1,
		"solving":    result,
		"reason":     mat.Reason,
	}
}
