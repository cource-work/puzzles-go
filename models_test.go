package main

import (
	"encoding/json"
	"reflect"
	"testing"
)

func TestNodeJSONMarshalling(t *testing.T) {
	// Create a sample Node object
	node := Node{I: 1, J: 2, Value: 3}

	// Marshal the Node object to JSON
	jsonData, err := json.Marshal(node)
	if err != nil {
		t.Errorf("Error marshalling Node to JSON: %v", err)
	}

	// Unmarshal the JSON data back to a Node object
	var unmarshalledNode Node
	err = json.Unmarshal(jsonData, &unmarshalledNode)
	if err != nil {
		t.Errorf("Error unmarshalling JSON to Node: %v", err)
	}

	// Check if the original and unmarshalled Node objects are equal
	if !reflect.DeepEqual(node, unmarshalledNode) {
		t.Errorf("Original and unmarshalled Node objects are not equal")
	}
}

func TestConditionJSONMarshalling(t *testing.T) {
	// Create a sample Condition object
	condition := Condition{X1: 1, Y1: 2, X2: 3, Y2: 4, Sum: 5}

	// Marshal the Condition object to JSON
	jsonData, err := json.Marshal(condition)
	if err != nil {
		t.Errorf("Error marshalling Condition to JSON: %v", err)
	}

	// Unmarshal the JSON data back to a Condition object
	var unmarshalledCondition Condition
	err = json.Unmarshal(jsonData, &unmarshalledCondition)
	if err != nil {
		t.Errorf("Error unmarshalling JSON to Condition: %v", err)
	}

	// Check if the original and unmarshalled Condition objects are equal
	if !reflect.DeepEqual(condition, unmarshalledCondition) {
		t.Errorf("Original and unmarshalled Condition objects are not equal")
	}
}

func TestMyRequestJSONMarshalling(t *testing.T) {
	// Create a sample MyRequest object
	request := MyRequest{
		N: 2,
		M: 3,
		Digits: []Node{
			{I: 1, J: 2, Value: 3},
			{I: 4, J: 5, Value: 6},
		},
		Conditions: []Condition{
			{X1: 1, Y1: 2, X2: 3, Y2: 4, Sum: 5},
			{X1: 6, Y1: 7, X2: 8, Y2: 9, Sum: 10},
		},
	}

	jsonData, err := json.Marshal(request)
	if err != nil {
		t.Errorf("Error marshalling MyRequest to JSON: %v", err)
	}

	// Unmarshal the JSON data back to a MyRequest object
	var unmarshalledRequest MyRequest
	err = json.Unmarshal(jsonData, &unmarshalledRequest)
	if err != nil {
		t.Errorf("Error unmarshalling JSON to MyRequest: %v", err)
	}

	// Check if the original and unmarshalled MyRequest objects are equal
	if !reflect.DeepEqual(request, unmarshalledRequest) {
		t.Errorf("Original and unmarshalled MyRequest objects are not equal")
	}
}

func TestUserJSONMarshalling(t *testing.T) {
	// Create a sample User object
	user := User{Username: "test", Password: "password"}

	// Marshal the User object to JSON
	jsonData, err := json.Marshal(user)
	if err != nil {
		t.Errorf("Error marshalling User to JSON: %v", err)
	}

	// Unmarshal the JSON data back to a User object
	var unmarshalledUser User
	err = json.Unmarshal(jsonData, &unmarshalledUser)
	if err != nil {
		t.Errorf("Error unmarshalling JSON to User: %v", err)
	}

	// Check if the original and unmarshalled User objects are equal
	if !reflect.DeepEqual(user, unmarshalledUser) {
		t.Errorf("Original and unmarshalled User objects are not equal")
	}
}
