package main

import (
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http"
	"strconv"
	"time"
)

var puzzles *mongo.Collection
var users *mongo.Collection

func userHandler(w http.ResponseWriter, r *http.Request) {
	// Check user authentication and authorization
	user, err := getCurrentUser(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	if err := checkUser(user); err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]interface{}{"user": user})
}

func getDataAndResultFromDatabase(c *gin.Context) {

	// Check user authentication and authorization
	userHandler(c.Writer, c.Request)

	// Extract id_item from URL parameters
	idItemStr := c.Param("id_item")
	idItem, err := strconv.Atoi(idItemStr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid id_item"})
		return
	}

	filter := bson.D{{"id_item", idItem}}
	var result map[string]interface{}
	err = puzzles.FindOne(context.Background(), filter).Decode(&result)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Item not found"})
		return
	}
	c.JSON(http.StatusOK, result)
}

func getGeneratedData(c *gin.Context) {
	// Check user authentication and authorization
	userHandler(c.Writer, c.Request)

	// MongoDB aggregation pipeline to find max id
	pipeline := mongo.Pipeline{
		{{"$group", bson.D{{"_id", nil}, {"max_id", bson.D{{"$max", "$id"}}}}}},
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	cursor, err := puzzles.Aggregate(ctx, pipeline)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	defer cursor.Close(ctx)

	var results []bson.M
	if err = cursor.All(ctx, &results); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	if len(results) == 0 {
		c.JSON(http.StatusNotFound, gin.H{"error": "No max_id found"})
		return
	}

	maxID := results[0]["max_id"].(int32)

	// Generate new data based on max_id
	newData := generateData(int(maxID))

	// Insert new data into the collection
	_, err = puzzles.InsertOne(ctx, newData)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Return the newly generated data
	c.JSON(http.StatusOK, newData)
}

func processData(c *gin.Context) {
	// Decode the request body into MyRequest struct
	var request MyRequest
	if err := c.BindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Check user authentication and authorization
	userHandler(c.Writer, c.Request)

	// Process the data using Matrix
	matrix := &Matrix{}
	if matrix.InitializeVariables(request) {
		startMatrix, result := matrix.ProcessData()
		response := map[string]interface{}{
			"matrix": startMatrix,
			"result": result,
		}
		c.JSON(http.StatusOK, response)
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Entered Data Is INVALID"})
	}
}
