package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func initDb() {
	ctx := context.TODO()

	serverAPI := options.ServerAPI(options.ServerAPIVersion1)
	opts := options.Client().ApplyURI("mongodb+srv://strelchenko2010amg:ojKwHwbQ27SRB48K@cluster0.d3kbadw.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0").SetServerAPIOptions(serverAPI)

	client, _ := mongo.Connect(ctx, opts)
	if err := client.Database("admin").RunCommand(context.TODO(), bson.D{{"ping", 1}}).Err(); err != nil {
		client.Disconnect(context.TODO())
	}
	log.Println("Pinged your deployment. You successfully connected to MongoDB!")

	puzzles = client.Database("strelchenko2010").Collection("conditions")
	users = client.Database("strelchenko2010").Collection("users")

}

func authenticateUser() gin.HandlerFunc {
	return func(c *gin.Context) {
		username, err := getCurrentUser(c.Request)
		fmt.Println(c.Request)
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
			c.Abort()
			return
		}

		if err := checkUser(username); err != nil {
			c.JSON(http.StatusForbidden, gin.H{"error": "Forbidden"})
			c.Abort()
			return
		}

		c.Set("username", username)
		c.Next()
	}
}

func main() {
	initDb()

	r := gin.Default()

	r.POST("/", createUser)
	r.POST("/login", login)

	authenticated := r.Group("/")
	authenticated.Use(authenticateUser())
	{
		authenticated.GET("/get-result/:id_item", getDataAndResultFromDatabase)
		authenticated.GET("/generate", getGeneratedData)
		authenticated.POST("/process_data", processData)
	}

	log.Println("Server listening on port 8514...")
	err := r.Run(":8514")
	if err != nil {
		return
	}
}
