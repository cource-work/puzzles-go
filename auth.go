package main

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/sessions"
	"log"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	_ "github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"golang.org/x/crypto/bcrypt"
)

var jwtKey = []byte("197b2c37c391bed93fe80344fe73b806947a65e36206e05a1a23c2fa12702fe3")
var store = sessions.NewCookieStore([]byte("my-session"))

type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type Claims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

func createUser(c *gin.Context) {

	var creds Credentials
	if err := json.NewDecoder(c.Request.Body).Decode(&creds); err != nil {
		http.Error(c.Writer, "Invalid request payload", http.StatusBadRequest)
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(creds.Password), bcrypt.DefaultCost)
	if err != nil {
		http.Error(c.Writer, "Error hashing password", http.StatusInternalServerError)
		return
	}

	user := bson.M{"username": creds.Username, "password": string(hashedPassword)}
	_, err = users.InsertOne(context.Background(), user)
	if err != nil {
		http.Error(c.Writer, "Error creating user", http.StatusInternalServerError)
		return
	}

	c.Writer.WriteHeader(http.StatusCreated)
}

func login(c *gin.Context) {

	var creds Credentials
	if err := json.NewDecoder(c.Request.Body).Decode(&creds); err != nil {
		http.Error(c.Writer, "Invalid request payload", http.StatusBadRequest)
		return
	}

	var user bson.M
	err := users.FindOne(context.Background(), bson.M{"username": creds.Username}).Decode(&user)
	if err != nil || bcrypt.CompareHashAndPassword([]byte(user["password"].(string)), []byte(creds.Password)) != nil {
		http.Error(c.Writer, "Invalid username or password", http.StatusUnauthorized)
		return
	}

	expirationTime := time.Now().Add(15 * time.Minute)
	claims := &Claims{
		Username: creds.Username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		http.Error(c.Writer, "Error creating token", http.StatusInternalServerError)
		return
	}

	c.Writer.Header().Set("Content-Type", "application/json")

	session, err := store.Get(c.Request, "my-session")
	if err != nil {
		http.Error(c.Writer, err.Error(), http.StatusInternalServerError)
		return
	}
	session.Values["token"] = tokenString
	err = session.Save(c.Request, c.Writer)
	if err != nil {
		http.Error(c.Writer, err.Error(), http.StatusInternalServerError)
		return
	}

	log.Println(c.Writer, "Token set in session")
	json.NewEncoder(c.Writer).Encode(map[string]string{
		"access_token": tokenString,
		"token_type":   "bearer",
	})
	c.Writer.WriteHeader(http.StatusOK)
}

func getCurrentUser(r *http.Request) (string, error) {
	session, _ := store.Get(r, "my-session")
	tokenStr := session.Values["token"].(string)
	if tokenStr == "" {
		return "", http.ErrNoCookie
	}

	claims := &Claims{}

	token, err := jwt.ParseWithClaims(tokenStr, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if err != nil || !token.Valid {
		return "", err
	}

	return claims.Username, nil
}

func checkUser(user string) error {
	if user == "" {
		return errors.New("authentication failed")
	}
	return nil
}
